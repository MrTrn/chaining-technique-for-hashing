/**
 * HashTabell.java
 * 
 * DA-ALG1000, Oblig 3, 2014
 * Terje Rene E. Nilsen - terje.nilsen@student.hive.no
 */
package tenilsen.oblig3;

/**
 *
 * @author Terje Rene E. Nilsen 
 * 
*/
public class HashTabell {
    private final int size;
    private final SingelLinkedList[] tabellen;
    
    public HashTabell (int theSize) {
        size = theSize;
        tabellen = new SingelLinkedList[size];
        initiateLists(); // initiating the SingleLinkedLists
    }
    
    // Fra oppgaven: "egen funksjon for hashing der indeksen finnes"
    private int doHash (int theNumber) {
        return Math.abs(theNumber % size);
    }
    
    private void initiateLists() {
        for (int i=0;i<size;i++) {
            tabellen[i] = new SingelLinkedList();
        }
    }
    
    public void add(int theNumber) {
        // System.out.print(theNumber % size + " " + theNumber); //debug
        tabellen[doHash(theNumber)].insertAtEnd(new Node(theNumber));
    }
    
    public int getSize() {
        return size;
    }
    
    
    @Override
    public String toString() {
        String theReturn ="";
        for (int i=0; i<size; i++) {
            
            theReturn += "["+i+"]:\n"+ tabellen[i].toStringSpecial() +"\n";
        }
        return theReturn;
    }
    
}
