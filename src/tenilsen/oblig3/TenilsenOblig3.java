/*
 * DA-ALG1000, Oblig 3,2014
 * Terje Rene E. Nilsen - terje.nilsen@student.hive.no
 * 
 * Oppgaven:
 * Skriv et program som benytter teknikken for ‘Chaining’ når det gjelder hashing. 
    Programmet skal lese inn lengden på tabellen som skal inneholde referanse til 
    hver enkelt lenket liste som skal genereres. Videre skal alle verdier, som skal 
    lagres, leses inn. En kjøring bør typisk lese inn ca. 15-20 verdier. Programmet 
    skal ha en egen funksjon for hashing der indeksen finnes. Når programmet har 
    generert de lenkete listene, skal teoretisk ‘load factor’ beregnes og skrives ut. 
    Hele tabellen skal skrives ut på enkel måte. Programmet skal benytte klassene for 
    SingelLinkedList og Node som du brukte i 1. obligatoriske oppgave. Rapport skal 
    leveres med og denne skal inneholde eksempel på kjøring av programmet. 
    Det er ikke nødvendig med GUI-løsning. * 
 */

package tenilsen.oblig3;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Terje Rene E. Nilsen
 */
public class TenilsenOblig3 {
    private static Scanner scannerInput;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  {
            // application magic here
            System.out.println("DA-ALG1000 - Oblig 3");
            System.out.println("Terje Rene E. Nilsen - terje.nilsen@student.hive.no\n");
            HashTabell theBuckets;
            int userChoice; // holding integer input from the user 
            boolean programRunning = true;
            
            while (programRunning) {
                try {
                    System.out.print("Enter tabell size:");
                    scannerInput = new Scanner(System.in);  
                    userChoice = Math.abs(scannerInput.nextInt());
                    theBuckets= new HashTabell(userChoice);
                    System.out.println("Tabell Size: " + userChoice);
                    System.out.print("How many values do you want to add? :");
                    scannerInput = new Scanner(System.in);  
                    int numbersOfValues = Math.abs(scannerInput.nextInt());
                    System.out.println("Numbers of values: " + numbersOfValues); //debug
                    for (int i=0; i < numbersOfValues; i++) {
                        // v1:
                        /*System.out.print("Enter number: ");
                        scannerInput = new Scanner(System.in);  
                        userChoice = scannerInput.nextInt();
                        */
                        /*
                        // v2:
                        userChoice = getNumberFromUser();
                        if (-9000000 == userChoice) {
                            // Atempt to save users from restarting
                            // the input session if non-integers are accedently entered..
                            // Also: wild guessing that this program wont be used for values <9000000
                            i--;
                        }
                        else {
                            theBuckets.add(userChoice);
                        }*/
                        
                        //#### v3:
                            try {
                                System.out.print("Enter number: ");
                                            scannerInput = new Scanner(System.in);  
                                userChoice = scannerInput.nextInt();
                                theBuckets.add(userChoice);
                            }
                            catch(InputMismatchException | NumberFormatException e){
                                // User entered something different from an integer
                                if ("end".equals(scannerInput.next())) { 
                                    // typing exit will exit the program
                                    // (old habits etc..)
                                    scannerInput.close(); i=numbersOfValues; //programRunning = false; 
                                }
                                else {
                                    System.out.println("Input error: " + "Only integers are accepted. \n" +
                                    "Please re-enter value or type exit to exit..");
                                    i--;
                                }
                            }
                        //####
                        
                    }
                    System.out.println("Good job!\n");
                    System.out.println("Load factor: " + 
                            (numbersOfValues/theBuckets.getSize()) );
                    System.out.println("The tabell:");
                    System.out.println(theBuckets.toString());
                    
                    System.out.println("\nType exit to exit, or continue to play more!");
                }
                catch(InputMismatchException | NumberFormatException e){
                    //\\NegativeArraySizeException
                    if ("exit".equals(scannerInput.next())) { 
                        // typing exit will exit the program
                        // (old habits etc..)
                        scannerInput.close(); programRunning = false; 
                    }
                    else {
                        // User entered something different from an integer
                        System.out.println("Input error: " + "Only integers are accepted \n" +
                            "Type exit to exit.");
                    }
                }
                /*catch(NullPointerException e){
                    // Bad programming.. For debugging..
                    System.out.println("NullPointerException! Avoided crash :)");
                    
                }//*/
            } // while(true) end
    }
    /*private static int getNumberFromUser() {
        try {
            System.out.print("Enter number: ");
                        scannerInput = new Scanner(System.in);  
            return scannerInput.nextInt();
        }
        catch(InputMismatchException | NumberFormatException e){
            // User entered something different from an integer
            System.out.println("Input error: " + "Only integers are accepted. \n" +
            "Please re-enter value..");
            return -9000000;
            }
            
    }*/
}
